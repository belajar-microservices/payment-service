/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.marketplace.payment.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author opaw
 */

@RestController
public class PaymentController {
    
    @GetMapping("/api/payment")
    public Map<String, Object> payment(@RequestParam String orderId){
        Map<String, Object> res = new HashMap<>();
        res.put("orderId", orderId);
        res.put("total", 50000);
        res.put("success", true);
        return res;
    }
    
}
